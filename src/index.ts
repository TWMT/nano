//
// Shared nanoservices framework.
//

import * as express from "express";
import * as amqp from "amqplib";
import * as request from "request";
import * as requestPromise from "request-promise";
import * as http from "http";
import * as helmet from "helmet";
const nocache = require("nocache");

import { loggerMiddleware, logger } from "./logger";
export { logger } from "./logger";
import { sleep, asyncHandler, retry } from "./utils";
export { sleep, asyncHandler, retry } from "./utils";

/**
 * Configures a nanoservice.
 */
export interface INanoServiceConfig {}

/**
 * Interface for responding to events.
 */
export interface IEventResponse {
  /**
   * Acknowledge that the event was successfully handled.
   */
  ack(): Promise<void>;
}

/**
 * Defines a potentially asynchronous callback function for handling an incoming event.
 */
export type EventHandlerFn<EventArgsT> = (
  eventArgs: EventArgsT,
  response: IEventResponse
) => Promise<void>;

/**
 * Defines a potentially asynchronous callback function for handling an incoming HTTP GET request.
 */
export type RequestHandlerFn = (
  request: express.Request,
  response: express.Response
) => Promise<void>;

/**
 * Interface that represents a particular nanoservice instance.
 */
export interface INanoService {
  /**
   * Returns true if the messaging system is currently available.
   */
  isMessagingAvailable(): boolean;

  closeHttpServer(): void;

  /**
   * Create a handler for a named incoming event from a direct queue.
   * Implemented by Rabbitmq under the hood for reliable messaging.
   *
   * @param eventName The name of the event to handle.
   * @param eventHandler Callback to be invoke when the incoming event is received.
   */
  on<EventArgsT = any>(
    eventName: string,
    eventHandler: EventHandlerFn<EventArgsT>
  ): Promise<void>;

  /**
   * Emit a named outgoing event.
   * Implemented by Rabbitmq under the hood for reliable messaging.
   *
   * @param eventName The name of the event to emit.
   * @param eventArgs Event args to publish with the event and be received at the other end.
   */
  emit<EventArgsT = any>(
    eventName: string,
    eventArgs: EventArgsT
  ): Promise<void>;

  /**
   * Create a handler for incoming HTTP GET requests.
   * Implemented by Express under the hood.
   */
  get(route: string, requestHandler: RequestHandlerFn): void;

  /**
   * Create a handler for incoming HTTP POST requests.
   * Implemented by Express under the hood
   *
   * @param route
   * @param requestHandler
   */
  post(route: string, requestHandler: RequestHandlerFn): void;

  /**
   * Create a handler for incoming HTTP DELETE requests.
   * Implemented by Express under the hood
   *
   * @param route
   * @param requestHandler
   */
  delete(route: string, requestHandler: RequestHandlerFn): void;

  /**
   * Create a handler for incoming HTTP PATCH requests.
   * Implemented by Express under the hood
   *
   * @param route
   * @param requestHandler
   */
  patch(route: string, requestHandler: RequestHandlerFn): void;

  /**
   * Create a handler for incoming HTTP PUT requests.
   * Implemented by Express under the hood
   *
   * @param route
   * @param requestHandler
   */
  put(route: string, requestHandler: RequestHandlerFn): void;

  /**
   * Make a request to another service.
   *
   * @param serviceName The name (logical or host) of the service.
   * @param route The HTTP route on the service to make the request to.
   * @param params Query parameters for the request.
   */
  request(serviceName: string, route: string, params?: any): Promise<any>;

  /**
   * Forward HTTP get request to another named service.
   * The response from the forward requests is automatically piped into the passed in response.
   *
   * @param serviceName The name of the service to forward the request to.
   * @param route The HTTP GET route to forward to.
   * @param body The body of the forwarded request.
   * @param response The response for the HTTP GET current request, to have the response forwarded to.
   */
  forwardRequest<RequestBodyT, ResponseT>(
    serviceName: string,
    route: string,
    body: RequestBodyT,
    response: express.Response
  ): void;

  /**
   * Forward HTTP get request to another named service.
   * The response from the forward requests is automatically piped into the passed in response.
   *
   * @param serviceName The name of the service to forward the request to.
   * @param route The HTTP GET route to forward to.
   * @param params Query parameters for the request.
   * @param res The stream to pipe response to.
   */
  forwardRequest2(
    serviceName: string,
    route: string,
    params: any,
    req: express.Request,
    res: express.Response
  ): void;

  /**
   * Setup serving of static files.
   *
   * @param dirPath The path to the directory that contains static files.
   */
  static(dirPath: string): void;

  /**
   * Reference to the timer interface.
   * Allows a service to time code for performance.
   */
  /**
   * Reference to the express object.
   */
  readonly expressApp: express.Express;

  /**
   * Reference to the HTTP server.
   */
  readonly httpServer: http.Server;

  /**
   * Starts the nanoservice.
   * It starts listening for incoming HTTP requests and events.
   */
  start(): Promise<void>;
}

//TODO: These should be passed into nano via options if necessary.
const inProduction = process.env.NODE_ENV === "production";
const host = process.env.HOST || "0.0.0.0";
const port = (process.env.PORT && parseInt(process.env.PORT)) || 3000;
const messagingHost =
  process.env.MESSAGING_HOST || "amqp://guest:guest@localhost:5672";

logger.info({ message: "Messagin HOST", host, port, messaging: messagingHost });
logger.info(
  `${inProduction ? "Running in Production" : "Running in development"}`
);
const defaultConfig: INanoServiceConfig = {};

//
// Used to register an event handler to be setup after messaging system has started.
//
interface IEventHandler {
  eventName: string;
  eventHandler: EventHandlerFn<any>;
}

//
// Class that represents a particular nanoservice instance.
//
class NanoService implements INanoService {
  //
  // RabbitMQ messaging connection.
  //
  private messagingConnection?: amqp.Connection;

  //
  // RabbitMQ messaging channel.
  //
  private messagingChannel?: amqp.Channel;

  //
  // Configuration for the nanoservice.
  //
  private config: INanoServiceConfig;

  //
  // Event handlers that have been registered to be setup once
  // connection to message queue is established.
  ///
  private registeredEventHandlers: IEventHandler[] = [];

  constructor(config?: INanoServiceConfig) {
    this.config = config || defaultConfig;
    if (!config) {
      logger.info("No config provided, using default.");
    }
    this.expressApp = express();
    this.httpServer = new http.Server(this.expressApp);

    this.expressApp.use(express.json({ limit: "300kb" }));

    // Disabling CSP by default. You can add your own via helmet.contentSecurityPolicy()
    this.expressApp.use(
      helmet({
        contentSecurityPolicy: false,
      })
    );

    // https://helmetjs.github.io/docs/crossdomain/
    this.expressApp.use(helmet.permittedCrossDomainPolicies());

    // https://helmetjs.github.io/docs/referrer-policy/
    this.expressApp.use(helmet.referrerPolicy({ policy: "same-origin" }));

    this.expressApp.get("/monitoring/alive", (req, res) => {
      res.json({ ok: true });
    });

    // It's possible that you've got bugs in an old HTML or JavaScript file, and with a cache, some users will be stuck with those old versions. This will (try to) abolish all client-side caching.
    this.expressApp.use(nocache());

    this.expressApp.use(loggerMiddleware);
  }

  //
  // Start the Express HTTP server.
  //
  private async startHttpServer(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.httpServer.listen(port, host, (err: any) => {
        if (err) {
          reject(err);
        } else {
          logger.info({ message: `Running on http://${host}:${port}` });
          resolve();
        }
      });
    });
  }

  //
  // Close the Express HTTP server.
  //
  closeHttpServer(): void {
    this.httpServer.close(() => {
      logger.info("Server closed!");
    });
  }

  //
  // Lazily start RabbitMQ messaging.
  //
  private async startMessaging(): Promise<void> {
    const initMessaging = async (): Promise<void> => {
      this.messagingConnection = await retry(
        async () => await amqp.connect(`${messagingHost}`),
        10000,
        1000
      );

      this.messagingConnection.on("error", (err) => {
        logger.error("Error from message system.");
        logger.error(err);
        logger.error({
          message: "Error from handler: MESSAGING route",
          error: (err && err.stack) || err,
        });
      });

      this.messagingConnection.on("close", (err) => {
        this.messagingConnection = undefined;
        this.messagingChannel = undefined;
        logger.info("Lost connection to rabbit, waiting for restart.");
        logger.error(err);
        initMessaging()
          .then(() => logger.info("Restarted messaging."))
          .catch((err) => {
            logger.error("Failed to restart messaging.");
            logger.error({
              message: "Error from handler: MESSAGING route",
              error: (err && err.stack) || err,
            });
          });
      });

      this.messagingChannel = await this.messagingConnection!.createChannel();

      for (const registeredEventHandler of this.registeredEventHandlers) {
        await this.internalOn(
          registeredEventHandler.eventName,
          registeredEventHandler.eventHandler
        );
      }
    };

    await initMessaging();

    //todo:
    // await connection.close();
  }

  /**
   * Returns true if the messaging system is currently available.
   */
  isMessagingAvailable(): boolean {
    return !!this.messagingConnection;
  }

  //
  // Internal message handler setup.
  //
  private async internalOn(
    eventName: string,
    eventHandler: EventHandlerFn<any>,
    verbose: boolean = true
  ): Promise<void> {
    // http://www.squaremobius.net/amqp.node/channel_api.html#channel_assertExchange
    await this.messagingChannel!.assertExchange(eventName, "fanout", {
      durable: true,
    });

    // http://www.squaremobius.net/amqp.node/channel_api.html#channel_assertQueue
    const queueName = (
      await this.messagingChannel!.assertQueue("", {
        durable: true,
        exclusive: true,
      })
    ).queue;
    logger.info("binding queue", queueName, "to", eventName);
    this.messagingChannel!.bindQueue(queueName, eventName, "");

    const messagingChannel = this.messagingChannel!;

    async function consumeCallback(msg: amqp.Message): Promise<void> {
      const args = JSON.parse(msg.content.toString());
      if (verbose) {
        logger.info(`Handling ${eventName}`);
        logger.info(args);
      }

      const eventResponse: IEventResponse = {
        async ack(): Promise<void> {
          messagingChannel.ack(msg);
        },
      };

      await eventHandler(args, eventResponse);

      if (verbose) {
        logger.info(`${eventName} handler done.`);
      }
    }

    logger.info(`Receiving events on queue ${eventName}`); //todo:

    // http://www.squaremobius.net/amqp.node/channel_api.html#channel_consume
    this.messagingChannel!.consume(
      queueName,
      asyncHandler(this, "ASYNC: " + eventName, consumeCallback),
      {
        noAck: false,
      }
    );
  }

  /**
   * Create a handler for a named incoming event.
   * Implemented by Rabbitmq under the hood for reliable messaging.
   *
   * @param eventName The name of the event to handle.
   * @param eventHandler Callback to be invoke when the incoming event is received.
   * @param verbose Display event logs
   */
  async on<EventArgsT = any>(
    eventName: string,
    eventHandler: EventHandlerFn<EventArgsT>,
    verbose: boolean = false
  ): Promise<void> {
    this.registeredEventHandlers.push({
      eventName: eventName,
      eventHandler: eventHandler,
    });

    if (this.messagingConnection) {
      //
      // Message system already started.
      //
      await this.internalOn(eventName, eventHandler, verbose);
    }
  }

  /**
   * Emit a named outgoing event.
   * Implemented by Rabbitmq under the hood for reliable messaging.
   *
   * @param eventName The name of the event to emit.
   * @param eventArgs Event args to publish with the event and be received at the other end.
   */
  async emit<EventArgsT = any>(
    eventName: string,
    eventArgs: EventArgsT
  ): Promise<void> {
    if (!this.messagingConnection) {
      throw new Error("Messaging system currently unavailable.");
    }

    // http://www.squaremobius.net/amqp.node/channel_api.html#channel_assertExchange
    await this.messagingChannel!.assertExchange(eventName, "fanout", {
      durable: true,
    });

    logger.info({ message: "emit message", eventArgs });

    // http://www.squaremobius.net/amqp.node/channel_api.html#channel_publish
    this.messagingChannel!.publish(
      eventName,
      "",
      new Buffer(JSON.stringify(eventArgs)),
      {
        persistent: true,
      }
    ); //TODO: Probably a more efficient way to do this! Maybe BSON?
  }

  /**
   * Create a handler for incoming HTTP GET requests.
   * Implemented by Express under the hood.
   */
  get(route: string, requestHandler: RequestHandlerFn): void {
    this.expressApp.get(
      route,
      (req: express.Request, res: express.Response) => {
        requestHandler(req, res)
          .then(() => {
            logger.info(`HTTP GET handler for ${route} finished.`);
          })
          .catch((err) => {
            logger.error(`Error from handler: HTTP GET route ${route} `);
            logger.error({
              message: `Error from handler: HTTP GET route ${route} `,
              error: (err && err.stack) || err,
            });

            res.sendStatus(500);
          });
      }
    );
  }

  //
  // POST request stub
  //
  post(route: string, requestHandler: RequestHandlerFn): void {
    this.expressApp.post(
      route,
      (req: express.Request, res: express.Response) => {
        requestHandler(req, res)
          .then(() => {
            logger.info(`HTTP POST handler for ${route} finished.`);
          })
          .catch((err) => {
            logger.error(`Error from handler: HTTP POST route ${route} `);
            logger.error({
              message: `Error from handler: HTTP POST route ${route} `,
              error: (err && err.stack) || err,
            });

            res.sendStatus(500);
          });
      }
    );
  }

  //
  // DELETE request stub
  //
  delete(route: string, requestHandler: RequestHandlerFn): void {
    this.expressApp.delete(
      route,
      (req: express.Request, res: express.Response) => {
        requestHandler(req, res)
          .then(() => {
            logger.info(`HTTP DELETE handler for ${route} finished.`);
          })
          .catch((err) => {
            logger.error(`Error from handler: HTTP DELETE route ${route} `);
            logger.error({
              message: `Error from handler: HTTP DELETE route ${route} `,
              error: (err && err.stack) || err,
            });

            res.sendStatus(500);
          });
      }
    );
  }

  //
  // PATCH request stub
  //
  patch(route: string, requestHandler: RequestHandlerFn): void {
    this.expressApp.patch(
      route,
      (req: express.Request, res: express.Response) => {
        requestHandler(req, res)
          .then(() => {
            logger.info(`HTTP PATCH handler for ${route} finished.`);
          })
          .catch((err) => {
            logger.error({
              message: "Error from handler: HTTP PATCH route",
              error: (err && err.stack) || err,
            });

            res.sendStatus(500);
          });
      }
    );
  }

  //
  // PUT request stub
  //
  put(route: string, requestHandler: RequestHandlerFn): void {
    this.expressApp.put(
      route,
      (req: express.Request, res: express.Response) => {
        requestHandler(req, res)
          .then(() => {
            logger.info(`HTTP PUT handler for ${route} finished.`);
          })
          .catch((err) => {
            logger.error(`Error from handler: HTTP PUT route ${route} `);
            logger.error({
              message: `Error from handler: HTTP PUT route ${route} `,
              error: (err && err.stack) || err,
            });

            res.sendStatus(500);
          });
      }
    );
  }

  //
  // Create a full URL for a service request mapping the service name to host name if necessary.
  //
  makeFullUrl(serviceName: string, route: string) {
    return "http://" + serviceName + route;
  }

  /**
   * Make a request to another service.
   *
   * @param serviceName The name (logical or host) of the service.
   * @param route The HTTP route on the service to make the request to.
   * @param params Query parameters for the request.
   */
  async request(
    serviceName: string,
    route: string,
    params?: any
  ): Promise<any> {
    let fullUrl = this.makeFullUrl(serviceName, route);
    if (params) {
      const paramKeys = Object.keys(params);
      let firstKey = true;
      for (let keyIndex = 0; keyIndex < paramKeys.length; ++keyIndex) {
        const key = paramKeys[keyIndex];
        const value = params[key];
        if (value !== undefined) {
          fullUrl += firstKey ? "?" : "&";
          fullUrl += key + "=" + value;
          firstKey = false;
        }
      }
    }

    logger.info(fullUrl); //TODO:

    return await requestPromise(fullUrl, { json: true });
  }

  /**
   * Forward HTTP get request to another named service.
   * The response from the forward requests is automatically piped into the passed in response.
   *
   * @param serviceName The name of the service to forward the request to.
   * @param route The HTTP GET route to forward to.
   * @param params Query parameters for the request.
   * @param toResponse The stream to pipe response to.
   */
  forwardRequest(
    serviceName: string,
    route: string,
    params: any,
    toResponse: express.Response
  ): void {
    //TODO: Get rid of this version.
    let fullUrl = this.makeFullUrl(serviceName, route);
    const paramKeys = Object.keys(params);
    let firstKey = true;
    for (let keyIndex = 0; keyIndex < paramKeys.length; ++keyIndex) {
      const key = paramKeys[keyIndex];
      const value = params[key];
      if (value) {
        fullUrl += firstKey ? "?" : "&";
        fullUrl += key + "=" + value;
        firstKey = false;
      }
    }

    logger.info(fullUrl);

    request(fullUrl).pipe(toResponse);
  }

  /**
   * Forward HTTP get request to another named service.
   * The response from the forward requests is automatically piped into the passed in response.
   *
   * @param serviceName The name of the service to forward the request to.
   * @param route The HTTP GET route to forward to.
   * @param params Query parameters for the request.
   * @param res The stream to pipe response to.
   */
  forwardRequest2(
    serviceName: string,
    route: string,
    params: any,
    req: express.Request,
    res: express.Response
  ): void {
    let fullUrl = this.makeFullUrl(serviceName, route);
    const paramKeys = Object.keys(params);
    let firstKey = true;
    for (let keyIndex = 0; keyIndex < paramKeys.length; ++keyIndex) {
      const key = paramKeys[keyIndex];
      const value = params[key];
      if (value) {
        fullUrl += firstKey ? "?" : "&";
        fullUrl += key + "=" + value;
        firstKey = false;
      }
    }

    logger.info(fullUrl);

    req.pipe(request(fullUrl)).pipe(res);
  }

  /**
   * Setup serving of static files.
   *
   * @param dirPath The path to the directory that contains static files.
   */
  static(dirPath: string): void {
    logger.info(`Serving static files from ${dirPath}`);
    this.expressApp.use(express.static(dirPath));
  }

  /**
   * Reference to the express object.
   */
  readonly expressApp: express.Express;

  /**
   * Reference to the HTTP server.
   */
  readonly httpServer: http.Server;

  /**
   * Starts the nanoservice.
   * It starts listening for incoming HTTP requests and events.
   */
  async start(): Promise<void> {
    await this.startHttpServer();

    await this.startMessaging(); //TODO: Would be good to optionally enable this.
  }
}

/**
 * Instantiates a nanoservice.
 *
 * @param [config] Optional configuration for the nanoservice.
 */
export function nano(config?: INanoServiceConfig): INanoService {
  return new NanoService(config);
}
