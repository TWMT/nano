import * as express from "express";
const winston = require("winston");
const { format, transports } = require("winston");
const { combine, timestamp } = format;
const LogzioWinstonTransport = require("winston-logzio");
const util = require("util");

const inProduction = process.env.NODE_ENV === "production";

export function factoryLogger() {
	// @TODO add more log types
	
	const combineMessageAndSplat = () => {
		return {
			transform: (info: any) => {
				//combine message and args if any
				info.message = util.format(
					info.message,
					...(info[Symbol.for("splat")] || [])
				);
				for(let key of Object.keys(info)){
					//Fixing winston bug
					//https://github.com/winstonjs/winston/issues/1408
					if(['message','metadata','level','timestamp','label'].indexOf(key)===-1){
						delete info[key]
					}
				}
				return info;
			},
		};
	};
	const serviceName = process.env.npm_package_name || "Unknown service";

	if (inProduction) {
		const logzioWinstonTransport = new LogzioWinstonTransport({
			name: "winston_logzio",
			token: process.env.SHIPPING_TOKEN || "",
			host: process.env.LISTENER_HOST || "",
		});
		return winston.createLogger({
			level: "info",
			format: winston.format.combine(
				format.label({label:serviceName,message:true}),
				combineMessageAndSplat(),
				winston.format.simple()
			),
			transports: [
				new transports.Console({ level: "debug", json: true }),
				logzioWinstonTransport,
			],
		});
	} else {
		return winston.createLogger({
			format: combine(
				format.label({label:serviceName,message:true}),
				combineMessageAndSplat(),
				format.json(),
				format.prettyPrint(),
				timestamp()
			),
			transports: [
				new transports.Console({ level: "silly", json: true }),
			],
		});
	}
}

export const logger = factoryLogger();

export const loggerMiddleware = (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
) => {
	// preventing log password
	const body = req.body ? req.body : {};
	const { password, tToken, mToken, token, ...logBody } = body;

	const toLog = {
		url: req.originalUrl,
		method: req.method,
		query: req.query,
		body: logBody,
		message: `HTTP request to: ${req.originalUrl}`,
		ip: req.ip,
		userAgent: req.get("User-Agent"),
	};

	logger.info(toLog);

	next();
};
